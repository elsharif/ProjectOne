# ProjectOne - ( Service over locations (SOL) )

Advertising Platform for advertisers to publish and manage their ads for thier clients.

The Impelmentation consists of two parts:

1 - Android app for both advertisers (to publish and manage the services and offers) and clients (to search and request the services and offers)

2 - RESTful API for backend operations, implements using ASP.NET Api 2 with C# and EntityFramework.



# Workflow:

![the workflow of the system](https://raw.githubusercontent.com/bhlshrf/ProjectOne/master/workflow.jpg)




# Demo:
click the picture to see the demo:
[![demo](https://img.youtube.com/vi/dSoHMzsk3Lg/0.jpg)](https://www.youtube.com/watch?v=dSoHMzsk3Lg "Demo")

# For more information:
You may want to see the 'Report.pdf' and 'Presentation.pptx'.
